﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.VehicleTest
{    
    /// <summary>
     /// This class includes car's behaviours like moving forward, turning, sounding
     /// </summary>
    public abstract class Vehicle
    {
        public Vehicle()
        {
            this.NumberOfWheels = 4;
            this.Direction = Direction.North;
            this.VehicleType = VehicleType.Car;
            CurrentPosition = new Point(0, 0);
        }
        /// <summary>
        /// Move the vehicle in the direction it is facing
        /// </summary>
        public void MoveForward(int units)
        {
            switch (this.Direction)
            {
                case Direction.East:
                    CurrentPosition = new Point(CurrentPosition.X + units, CurrentPosition.Y);
                    break;
                case Direction.South:
                    CurrentPosition = new Point(CurrentPosition.X , CurrentPosition.Y - units);
                    break;
                case Direction.West:
                    CurrentPosition = new Point(CurrentPosition.X -units, CurrentPosition.Y );
                    break;
                case Direction.North:
                    CurrentPosition = new Point(CurrentPosition.X, CurrentPosition.Y + units);
                    break;
            }
        }

        /// <summary>
        /// Turn vehicle 90deg right
        /// </summary>
        public void TurnRight()
        {
            if (Direction == Direction.North)
                Direction = Direction.East;
            else
                Direction = Direction + 1;

        }

        /// <summary>
        /// Turn vehicle 90deg left
        /// </summary>
        public void TurnLeft()
        {
            if (Direction == Direction.East)
                Direction = Direction.North;
            else
                Direction = Direction - 1;
        }

        /// <summary>
        /// Sound the vehicle horn
        /// </summary>
        public virtual string SoundHorn()
        {
            return "Bi,Bi...I am a " + VehicleType.ToString();
        }

        // Get and Return the current position of the vehicle
        public Point CurrentPosition { get; set; }

        //  Get and Return the number of wheels on the vehicle
        public int NumberOfWheels { get; protected set; }

        //  Get and Return the type of vehicle we are using
        public VehicleType VehicleType { get; protected set; }

        //  Get and Return the direction of vehicle we are using
        public Direction Direction { get; protected set; }
    }

    /// <summary>
    /// Type of direction
    /// </summary>
    public enum Direction
    {
        East = 0,
        South = 1,
        West = 2,
        North = 3
    }

    /// <summary>
    /// This Class car inherits the Class vehicle and interface Ivehicle
    /// </summary>
    public class Car: Vehicle ,IVehicle
    {
        /// <summary>
        /// Consturctor of Car
        /// </summary>
        public Car()
        {
            this.NumberOfWheels = 4;
            VehicleType = VehicleType.Car;
        }

        /// <summary>
        /// Over ride the soundhorn function for car
        /// </summary>
        public override string SoundHorn()
        {
            return "honk";
        }
    }

    /// <summary>
    /// This Class Truck inherits the Class vehicle and interface Ivehicle
    /// </summary>
    public class Truck : Vehicle, IVehicle
    {
        /// <summary>
        /// Consturctor of Truck
        /// </summary>
        public Truck()
        {
            this.NumberOfWheels = 4;
            VehicleType = VehicleType.Truck;
        }

        /// <summary>
        /// Over ride the soundhorn function for truck
        /// </summary>
        public override string SoundHorn()
        {
            return "braap";
        }
    }

    /// <summary>
    /// This Class moterbike inherits the Class vehicle and interface Ivehicle
    /// </summary>
    public class Motorbike : Vehicle, IVehicle
    {
        /// <summary>
        /// Consturctor of moterbike
        /// </summary>
        public Motorbike()
        {
            this.NumberOfWheels = 2;
            VehicleType = VehicleType.Motorbike;
        }

        /// <summary>
        /// Over ride the soundhorn function for motorbike
        /// </summary>
        public override string SoundHorn()
        {
            return "beep";
        }
    }


}
