The test is written in C#.NET and you can open it using Visual Studio or [Visual Studio Community Edition](https://www.visualstudio.com/vs/community/) 
if you do not own a copy of visual studio.  

### Project Structure

The basic tests lie in the `Tests` Project. Each folder inside the project 
contains all the relevant classes / code required to complete the test. 
To begin inspect the `Test.cs` file under each test to see what the 
parameters of the test are.