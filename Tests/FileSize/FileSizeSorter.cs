﻿using System.Collections.Generic;
using Framework;
using System.Collections;
using System;

using System.Linq;
using System.Text.RegularExpressions;

namespace Tests.FileSize
{
    public class FileSizeSorter
    {
        /// <summary>
        /// Sort a list of File Size strings by the value they represent
        /// </summary>
        /// <remarks>
        /// We'd like this method like to sort a given list of strings that represent file sizes (eg. "23.5 kB")
        /// 
        /// File sizes may come with decimals or without, will have whitespace seperating the number and unit
        /// and will use the following units (In Descending order):
        ///     - TB  (TeraByte) - 1024^4 - 1,099,511,627,776 Bytes
        ///     - GB  (GigaByte) - 1024^3 - 1,073,741,824     Bytes
        ///     - MB  (MegaByte) - 1024^2 - 1,048,576         Bytes
        ///     - kB  (KiloByte) - 1024   - 1,024             Bytes
        ///     - B   (Byte)     - 1      - 1                 Bytes
        /// 
        /// See Unit Tests for more details.
        /// </remarks>
        /// <param name="fileSizes">An enumerable of strings containing a value that represents a filesize as described above.</param>
        /// <param name="descending">A boolean determining if the results should be in descending order.</param>
        /// <returns>
        /// An enumerable of strings (same values provided) sorted by the file-sizes that they represent.
        /// </returns>
        public virtual IEnumerable<string> Sort(IEnumerable<string> fileSizes, bool descending = false)
        {
            List<FileSize> FileSizeList = new List<FileSize>();

            foreach (string filesize in fileSizes)
            {
                //store file size string to Class FileSize object
                FileSize size = new FileSize(filesize);

                // Remove leading and trailing white spaces
                string temp = filesize.TrimStart();
                temp = temp.TrimEnd();
            
                //store the file size and unit to FileSize object
                store(temp, size);

                //Convert the file size from TB, GB,MB and kB to B
                ConvertSize(size, FileSizeList);

            }

            //Sort the FileSizeList
            if(descending)
                FileSizeList.Sort(delegate (FileSize c1, FileSize c2) { return c2.Value.CompareTo(c1.Value); });
            else
                FileSizeList.Sort(delegate (FileSize c1, FileSize c2) { return c1.Value.CompareTo(c2.Value); });

            //return the sorted file size
            foreach (FileSize file in FileSizeList)
            {
                yield return file.Text;
            }            

        }

        /// <summary>
        /// Declare a FileSize class to store the size and Unit
        /// </summary>
        public class FileSize
        {
            public FileSize(string text)
            {
                this.Text = text;
            }
            public string Unit { get; set; }
            public decimal Value { get; set; }

            public string Text { get; set; }
        }

        /// <summary>
        /// Extract the number from the string using regular expression
        /// </summary>
        public static decimal GetNumber(string str)
        {
            decimal result = 0;
            string r = Regex.Replace(str, @"[^0-9.]+", "");
            r = Regex.Replace(r, @"(?<=\..*?)\.", "");
            result = decimal.Parse(r);
            return result;
        }

        /// <summary>
        /// Convert the file size
        /// </summary>
        /// <remark>
        /// Convert the file size from TB, GB,MB and kB to B
        /// </remark>
        public void ConvertSize(FileSize size, List<FileSize> FileSizeList)
        {
            switch (size.Unit)
            {
                case "TB":
                    size.Value *= Convert.ToDecimal(Math.Pow(1024, 4));
                    FileSizeList.Add(size);
                    break;
                case "GB":
                    size.Value *= Convert.ToDecimal(Math.Pow(1024, 3));
                    FileSizeList.Add(size);
                    break;
                case "MB":
                    size.Value *= 1024 * 1024;
                    FileSizeList.Add(size);
                    break;
                case "kB":
                    size.Value *= 1024;
                    FileSizeList.Add(size);
                    break;
                case "B":
                    size.Value *= 1;
                    FileSizeList.Add(size);
                    break;
            }
        }

        /// <summary>
        /// Store the file size value and unit to FileSize class 
        /// </summary>
        public void store(string temp, FileSize size)
        {
            if (temp.Substring(temp.Length - 2, 1) == "T" || temp.Substring(temp.Length - 2, 1) == "G" || temp.Substring(temp.Length - 2, 1) == "M" || temp.Substring(temp.Length - 2, 1) == "k")
            {
                size.Unit = temp.Substring(temp.Length - 2);
                size.Value = GetNumber(temp);
            }
            else
            {
                size.Unit = temp.Substring(temp.Length - 1);
                size.Value = GetNumber(temp);
            }
        }

    }
}
